<?php

use App\Http\Controllers\JWTController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'apiJwt'], function($router) {
    Route::post('/register', [JWTController::class, 'register']);
    Route::post('/login', [JWTController::class, 'login']);
    Route::post('/logout', [JWTController::class, 'logout']);
    Route::post('/refresh', [JWTController::class, 'refresh']);
    Route::post('/profile', [JWTController::class, 'profile']);

    Route::apiResource('paciente', 'App\Http\Controllers\PacienteController');
    Route::apiResource('procedimento', 'App\Http\Controllers\ProcedimentoController');
    Route::apiResource('especialidade', 'App\Http\Controllers\EspecialidadeController');
    Route::apiResource('medico', 'App\Http\Controllers\MedicoController');
    Route::apiResource('plano-saude', 'App\Http\Controllers\PlanoSaudeController');
    Route::apiResource('consulta', 'App\Http\Controllers\ConsultaController');
    Route::apiResource('paciente-plano-saude', 'App\Http\Controllers\PacientePlanoSaudeController');
    Route::apiResource('consulta-paciente', 'App\Http\Controllers\ConsultaPacienteController');
    Route::apiResource('consulta-medico', 'App\Http\Controllers\ConsultaMedicoController');
    Route::apiResource('medico-especialidade', 'App\Http\Controllers\MedicoEspecialidadeController');
    Route::apiResource('consulta-procedimento', 'App\Http\Controllers\ConsultaProcedimentoController');
});

