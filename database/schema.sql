-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ti_saude
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ti_saude` ;

-- -----------------------------------------------------
-- Schema ti_saude
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ti_saude` DEFAULT CHARACTER SET utf8 ;
USE `ti_saude` ;

-- -----------------------------------------------------
-- Table `plano_saude`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `plano_saude` ;

CREATE TABLE IF NOT EXISTS `plano_saude` (
  `plan_codigo` INT NOT NULL AUTO_INCREMENT,
  `plano_descricao` VARCHAR(60) NOT NULL,
  `plano_telefone` VARCHAR(15) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`plan_codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `procedimento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `procedimento` ;

CREATE TABLE IF NOT EXISTS `procedimento` (
  `proc_codigo` INT NOT NULL AUTO_INCREMENT,
  `proc_nome` VARCHAR(60) NOT NULL,
  `proc_valor` DECIMAL(6,2) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`proc_codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paciente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paciente` ;

CREATE TABLE IF NOT EXISTS `paciente` (
  `pac_codigo` INT NOT NULL AUTO_INCREMENT,
  `pac_nome` VARCHAR(100) NOT NULL,
  `pac_dataNascimento` DATE NOT NULL,
  `pac_telefone` VARCHAR(15) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`pac_codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `especialidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `especialidade` ;

CREATE TABLE IF NOT EXISTS `especialidade` (
  `espec_codigo` INT NOT NULL AUTO_INCREMENT,
  `espec_nome` VARCHAR(60) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`espec_codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `medico` ;

CREATE TABLE IF NOT EXISTS `medico` (
  `med_codigo` INT NOT NULL AUTO_INCREMENT,
  `med_CRM` VARCHAR(10) NOT NULL,
  `med_nome` VARCHAR(60) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`med_codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `consulta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `consulta` ;

CREATE TABLE IF NOT EXISTS `consulta` (
  `cons_codigo` INT NOT NULL AUTO_INCREMENT,
  `data` DATE NOT NULL,
  `hora` TIME NOT NULL,
  `particular` VARCHAR(10) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`cons_codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medico_especialidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `medico_especialidade` ;

CREATE TABLE IF NOT EXISTS `medico_especialidade` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `med_codigo` INT NOT NULL,
  `espec_codigo` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `fk_med_espec_especialidade`
    FOREIGN KEY (`espec_codigo`)
    REFERENCES `especialidade` (`espec_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_med_espec_medico1`
    FOREIGN KEY (`med_codigo`)
    REFERENCES `medico` (`med_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_med_espec_especialidade_idx` ON `medico_especialidade` (`espec_codigo` ASC);

CREATE INDEX `fk_med_espec_medico1_idx` ON `medico_especialidade` (`med_codigo` ASC);


-- -----------------------------------------------------
-- Table `consulta_medico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `consulta_medico` ;

CREATE TABLE IF NOT EXISTS `consulta_medico` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `cons_codigo` INT NOT NULL,
  `med_codigo` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `fk_consulta_medico_medico1`
    FOREIGN KEY (`med_codigo`)
    REFERENCES `medico` (`med_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_consulta_medico_consulta1`
    FOREIGN KEY (`cons_codigo`)
    REFERENCES `consulta` (`cons_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_consulta_medico_medico1_idx` ON `consulta_medico` (`med_codigo` ASC);

CREATE INDEX `fk_consulta_medico_consulta1_idx` ON `consulta_medico` (`cons_codigo` ASC);


-- -----------------------------------------------------
-- Table `consulta_procedimento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `consulta_procedimento` ;

CREATE TABLE IF NOT EXISTS `consulta_procedimento` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `cons_codigo` INT NOT NULL,
  `proc_codigo` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `fk_table1_procedimento1`
    FOREIGN KEY (`proc_codigo`)
    REFERENCES `procedimento` (`proc_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_consulta1`
    FOREIGN KEY (`cons_codigo`)
    REFERENCES `consulta` (`cons_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_table1_procedimento1_idx` ON `consulta_procedimento` (`proc_codigo` ASC);

CREATE INDEX `fk_table1_consulta1_idx` ON `consulta_procedimento` (`cons_codigo` ASC);


-- -----------------------------------------------------
-- Table `consulta_paciente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `consulta_paciente` ;

CREATE TABLE IF NOT EXISTS `consulta_paciente` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `pac_codigo` INT NOT NULL,
  `cons_codigo` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `fk_consulta_paciente_paciente1`
    FOREIGN KEY (`pac_codigo`)
    REFERENCES `paciente` (`pac_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_consulta_paciente_consulta1`
    FOREIGN KEY (`cons_codigo`)
    REFERENCES `consulta` (`cons_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_consulta_paciente_paciente1_idx` ON `consulta_paciente` (`pac_codigo` ASC);

CREATE INDEX `fk_consulta_paciente_consulta1_idx` ON `consulta_paciente` (`cons_codigo` ASC);


-- -----------------------------------------------------
-- Table `paciente_plano_saude`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `paciente_plano_saude` ;

CREATE TABLE IF NOT EXISTS `paciente_plano_saude` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nr_contrato` VARCHAR(50) NOT NULL,
  `pac_codigo` INT NOT NULL,
  `plan_codigo` INT NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `fk__plano_saude1`
    FOREIGN KEY (`plan_codigo`)
    REFERENCES `plano_saude` (`plan_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk__paciente1`
    FOREIGN KEY (`pac_codigo`)
    REFERENCES `paciente` (`pac_codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk__plano_saude1_idx` ON `paciente_plano_saude` (`plan_codigo` ASC);

CREATE INDEX `fk__paciente1_idx` ON `paciente_plano_saude` (`pac_codigo` ASC);


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users` ;

CREATE TABLE IF NOT EXISTS `users` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
