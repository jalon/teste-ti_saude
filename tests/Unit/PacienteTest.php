<?php

namespace Tests\Unit;

use App\Repositories\PacienteRepository;
use App\Services\PacienteService;
use Illuminate\Http\Request;
use Mockery;
use Tests\TestCase;

class PacienteTest extends TestCase
{
    /**
     * Teste para verificar se o repository será invocado no método listar do service
     *
     * @return void
     */
    public function testListar()
    {
        $this->instance(PacienteRepository::class, Mockery::mock(PacienteRepository::class, function ($mock) {
            $mock->shouldReceive('listar')
                ->once();
        }));

        $service = app(PacienteService::class);
        $service->listar();
    }

    /**
     * Teste para verificar se o repository será invocado no método cadastrar do service
     *
     * @return void
     */
    public function testCadastrar()
    {
        $this->instance(PacienteRepository::class, Mockery::mock(PacienteRepository::class, function ($mock) {
            $mock->shouldReceive('cadastrar')
                ->once();
        }));

        $service = app(PacienteService::class);

        $request = new Request();
        $request->replace([
            'pac_nome' => 'teste',
            'pac_dataNascimento' => '1999-01-01',
            'pac_telefone' => '8299999999'
        ]);

        $service->cadastrar($request);
    }

    /**
     * Teste para verificar se o repository será invocado no método editar do service
     *
     * @return void
     */
    public function testEditar()
    {
        $this->instance(PacienteRepository::class, Mockery::mock(PacienteRepository::class, function ($mock) {
            $mock->shouldReceive('editar')
                ->once();
        }));

        $service = app(PacienteService::class);

        $request = new Request();
        $request->replace([
            'pac_nome' => 'teste 2',
            'pac_dataNascimento' => '1999-01-01',
            'pac_telefone' => '8299999999'
        ]);

        $service->editar(1, $request);
    }

    /**
     * Teste para verificar se o repository será invocado no método excluir do service
     *
     * @return void
     */
    public function testExcluir()
    {
        $this->instance(PacienteRepository::class, Mockery::mock(PacienteRepository::class, function ($mock) {
            $mock->shouldReceive('excluir')
                ->once();
        }));

        $service = app(PacienteService::class);
        $service->excluir(1);
    }
}
