<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultaMedico extends Model
{
    protected $table        = 'consulta_medico';
    protected $primaryKey   = 'codigo';

    protected $fillable = [
        'codigo',
        'cons_codigo',
        'med_codigo'
    ];
}
