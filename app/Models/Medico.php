<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    protected $table        = 'medico';
    protected $primaryKey   = 'med_codigo';

    protected $fillable = [
        'med_codigo',
        'med_CRM',
        'med_nome'
    ];
}
