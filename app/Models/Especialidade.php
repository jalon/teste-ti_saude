<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Especialidade extends Model
{
    protected $table        = 'especialidade';
    protected $primaryKey   = 'espec_codigo';

    protected $fillable = [
        'espec_codigo',
        'espec_nome'
    ];
}
