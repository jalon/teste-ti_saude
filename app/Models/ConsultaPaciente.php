<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultaPaciente extends Model
{
    protected $table = 'consulta_paciente';
    protected $primaryKey   = 'codigo';

    protected $fillable = [
        'codigo',
        'pac_codigo',
        'cons_codigo'
    ];
}
