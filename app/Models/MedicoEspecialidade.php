<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicoEspecialidade extends Model
{
    protected $table = 'medico_especialidade';
    protected $primaryKey   = 'codigo';

    protected $fillable = [
        'codigo',
        'med_codigo',
        'espec_codigo'
    ];
}
