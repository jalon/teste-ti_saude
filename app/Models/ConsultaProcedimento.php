<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultaProcedimento extends Model
{
    protected $table = 'consulta_procedimento';
    protected $primaryKey   = 'codigo';

    protected $fillable = [
        'codigo',
        'cons_codigo',
        'proc_codigo'
    ];
}
