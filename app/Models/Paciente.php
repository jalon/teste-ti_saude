<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table        = 'paciente';
    protected $primaryKey   = 'pac_codigo';

    protected $fillable = [
        'pac_codigo',
        'pac_nome',
        'pac_dataNascimento',
        'pac_telefone'
    ];
}
