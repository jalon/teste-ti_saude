<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Procedimento extends Model
{
    protected $table        = 'procedimento';
    protected $primaryKey   = 'proc_codigo';

    protected $fillable = [
        'proc_codigo',
        'proc_nome',
        'proc_valor'
    ];
}
