<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanoSaude extends Model
{
    protected $table        = 'plano_saude';
    protected $primaryKey   = 'plan_codigo';

    protected $fillable = [
        'plan_codigo',
        'plano_descricao',
        'plano_telefone'
    ];
}
