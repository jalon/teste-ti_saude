<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    protected $table        = 'consulta';
    protected $primaryKey   = 'cons_codigo';

    protected $fillable = [
        'cons_codigo',
        'data',
        'hora',
        'particular'
    ];
}
