<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PacientePlanoSaude extends Model
{
    protected $table = 'paciente_plano_saude';
    protected $primaryKey   = 'codigo';

    protected $fillable = [
        'codigo',
        'nr_contrato',
        'pac_codigo',
        'plan_codigo'
    ];
}
