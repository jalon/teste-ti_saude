<?php

namespace App\Services;

use App\Repositories\ConsultaRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ConsultaService
{
    protected ConsultaRepository $repository;

    public function __construct(ConsultaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listar(int $porPagina = 5, string $direcao = null, string $ordenarPor = null, string $criterioBusca = null, string $filtros = null): JsonResponse
    {
        $retorno = $this->repository->listar($porPagina, $direcao, $ordenarPor, $criterioBusca, $filtros);
        return response()->json($retorno, Response::HTTP_OK);
    }

    public function cadastrar(Request $request): JsonResponse
    {
        $retorno = $this->repository->cadastrar($request);

        if (!is_null($retorno)) {
            return response()->json([
                'data' => true,
                'message' => 'Registro cadastrado com sucesso.',
                'notification' => null,
            ], Response::HTTP_CREATED);
        }

        return response()->json([
            'data' => null,
            'notification' => null,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function editar(string $id, Request $request): JsonResponse
    {
        $model = $this->repository->editar($id, $request);

        if (is_object($model)) {
            return response()->json([
                'data' => true,
                'message' => 'Registro atualizado com sucesso.',
                'notification' => null,
            ], Response::HTTP_OK);
        }

        return response()->json([
            'data' => null,
            'notification' => null,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function excluir(string $id): JsonResponse
    {
        $retorno = $this->repository->excluir($id);

        if ($retorno) {
            return response()->json([
                'data' => null,
                'notification' => null,
                'message' => 'Registro excluído com sucesso.'
            ], Response::HTTP_ACCEPTED);
        }

        return response()->json([
            'data' => null,
            'notification' => null,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
