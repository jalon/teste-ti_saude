<?php

namespace App\Repositories;

use App\Models\ConsultaMedico;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class ConsultaMedicoRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'cons_codigo';

    public function __construct()
    {
        $this->model = app(ConsultaMedico::class);
    }

}
