<?php

namespace App\Repositories;

use App\Models\Medico;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class MedicoRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'med_nome';

    public function __construct()
    {
        $this->model = app(Medico::class);
    }

}
