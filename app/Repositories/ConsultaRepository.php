<?php

namespace App\Repositories;

use App\Models\Consulta;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class ConsultaRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'data';

    public function __construct()
    {
        $this->model = app(Consulta::class);
    }

}
