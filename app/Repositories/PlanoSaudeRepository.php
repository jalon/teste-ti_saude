<?php

namespace App\Repositories;

use App\Models\PlanoSaude;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class PlanoSaudeRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'plano_descricao';

    public function __construct()
    {
        $this->model = app(PlanoSaude::class);
    }

}
