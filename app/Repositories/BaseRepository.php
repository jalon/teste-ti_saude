<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Exception;

abstract class BaseRepository
{
    protected object $model;
    protected string $sortBy;
    protected array $propertiesToSearch;

    public function __construct()
    {

    }

    public function listar(int $porPagina = 5, string $direcao = null, string $ordenarPor = null, string $criterioBusca = null, string $filtros = null): array
    {
        $consulta = $this->model::orderBy(
                $ordenarPor ?? $this->sortBy,
                $direcao == 'descending' ? 'desc' : 'asc'
            );

        if (isset($filtros)) {
            collect(array_keys((array)json_decode($filtros)))->map(function ($column, $key) use ($consulta, $filtros) {
                if(!array_values((array)json_decode($filtros))[$key]) {
                    return $consulta;
                }
                return  $consulta->where($column, 'LIKE', "%" . array_values((array)json_decode($filtros))[$key] . "%");
            })->reject(function ($column) {
                return empty($column);
            });
        }

        if ($criterioBusca) {
            foreach ($this->propertiesToSearch as $key => $propriedade) {
                if ($key == 0) {
                    $consulta->where($propriedade, 'LIKE', "%$criterioBusca%");
                } else {
                    $consulta->orWhere($propriedade, 'LIKE', "%$criterioBusca%");
                }
            }
        }

        $result = $consulta->paginate((int) $porPagina, $this->listFields(),  'current_page');

        return [
            "current_page" => $result->currentPage(),
            "data" => $result->getCollection(),
            "per_page" => $result->perPage(),
            "total" => $result->total(),
            "search" => $criterioBusca,
            "filters" => $filtros ? json_decode($filtros) : [],
            "order" => $direcao,
            "sort" => $ordenarPor ?? $this->sortBy,
        ];
    }

    public function cadastrar(Request $request): ?object
    {
        try {
            $model = $this->model->create($request->all());
            return $model;
        } catch (Exception $e) {
            \Log::error($e);
            return null;
        }
    }

    public function editar(string $id, Request $request): ?object
    {
        try {
            $model = $this->model->find($id);
            $form = $request->all();

            if ($model->fill($form)->getAttributes() != $model->getRawOriginal()) {
                $model->fill($form)->save();
            }

            return $model;
        } catch (Exception $e) {
            \Log::error($e);
            return null;
        }
    }

    public function excluir(string $id): bool
    {
        try {
            $result = $this->model->findOrFail($id);
            $result->delete();

            return true;
        } catch (Exception $e) {
            \Log::error($e);
            return false;
        }
    }

    public function buscarPor(string $column, string $value, bool $log = false): ?object
    {
        try {
            $query = $this->model->where($column, '=', $value)->first();

            return  $query;
        } catch (Exception $e) {
            \Log::error($e);
            return null;
        }
    }

    public function options(int $porPagina = 5): array
    {
        return [
            "current_page" => 1,
            "data" =>  [],
            "per_page" => (int) $porPagina,
            "total" => 0,
            "search" => '',
            "order" => '',
            "sort" => 'asc'
        ];
    }

    public function listFields(): array
    {
        return ['*'];
    }
}
