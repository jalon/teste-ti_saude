<?php

namespace App\Repositories;

use App\Models\Especialidade;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class EspecialidadeRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'espec_nome';

    public function __construct()
    {
        $this->model = app(Especialidade::class);
    }

}
