<?php

namespace App\Repositories;

use App\Models\MedicoEspecialidade;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class MedicoEspecialidadeRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'med_codigo';

    public function __construct()
    {
        $this->model = app(MedicoEspecialidade::class);
    }

}
