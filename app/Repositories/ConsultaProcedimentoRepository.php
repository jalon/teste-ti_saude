<?php

namespace App\Repositories;

use App\Models\ConsultaProcedimento;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class ConsultaProcedimentoRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'cons_codigo';

    public function __construct()
    {
        $this->model = app(ConsultaProcedimento::class);
    }

}
