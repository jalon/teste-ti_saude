<?php

namespace App\Repositories;

use Illuminate\Http\Request;

interface BaseRepositoryInterface
{
    public function listar(
        int $porPagina = 5,
        string $direcao = null,
        string $ordenarPor = null,
        string $criterioBusca = null,
        string $filtros = null
    ): array;
    public function cadastrar(Request $request): ?object;
    public function editar(string $id, Request $request): ?object;
    public function buscarPor(string $column, string $value, bool $log = false): ?object;
    public function excluir(string $id): bool;
    public function options(int $porPagina = 5): array;
    public function listFields(): array;
}
