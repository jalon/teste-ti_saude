<?php

namespace App\Repositories;

use App\Models\Paciente;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class PacienteRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'pac_nome';

    public function __construct()
    {
        $this->model = app(Paciente::class);
    }

}
