<?php

namespace App\Repositories;

use App\Models\ConsultaPaciente;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class ConsultaPacienteRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'pac_codigo';

    public function __construct()
    {
        $this->model = app(ConsultaPaciente::class);
    }

}
