<?php

namespace App\Repositories;

use App\Models\PacientePlanoSaude;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class PacientePlanoSaudeRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'nr_contrato';

    public function __construct()
    {
        $this->model = app(PacientePlanoSaude::class);
    }

}
