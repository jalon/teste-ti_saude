<?php

namespace App\Repositories;

use App\Models\Procedimento;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;


class ProcedimentoRepository extends BaseRepository implements BaseRepositoryInterface
{
    protected object $model;
    protected string $sortBy = 'proc_nome';

    public function __construct()
    {
        $this->model = app(Procedimento::class);
    }

}
