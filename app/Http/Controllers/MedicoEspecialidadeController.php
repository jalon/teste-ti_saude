<?php

namespace App\Http\Controllers;

use App\Services\MedicoEspecialidadeService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class MedicoEspecialidadeController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var MedicoEspecialidadeService $service
    */
    protected $service;

    public function __construct(MedicoEspecialidadeService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request): JsonResponse
    {
        return $this->service->listar($request->per_page ?? 5, $request->order, $request->sort, $request->search, $request->filters);
    }

    public function store(Request $request): JsonResponse
    {
        return $this->service->cadastrar($request);
    }

    public function update(string $id, Request $request): JsonResponse
    {
        return $this->service->editar($id, $request);
    }

    public function destroy(string $id): JsonResponse
    {
        return $this->service->excluir($id);
    }
}
