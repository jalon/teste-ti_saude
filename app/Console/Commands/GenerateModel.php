<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Version 1.3.2
 * [17-02-22]
 */

class GenerateModel extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $signature = 'generate:model {name} {--table=} {--force}';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Generate a new model class';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Model';

    /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        $name = $this->argument('name');

        if(is_array($name)){
            $name = $name[0];
        } elseif(is_null($name) || !$name){
            $name = '';
        }

        $string = strrchr($name, '/');
        $string = $string ? $string : '';

        return str_replace('ExemploModel', substr($string, 1) ? substr($string, 1) : $name, $stub);
    }

    /**
     * Altera o namespace correspondente
     *
     * @param string $name
     * @return $this|string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceNamespace($stub, $name)
            ->replaceModel($stub, $name)
            ->replaceClass($stub, $name);
    }

    /**
     * Captura as informacoes do banco e adiciona no modelo
     *
     * @param string $stub
     * @param string $name
     * @return $this
     */
    protected function replaceModel(&$stub, $name)
    {
        $table = $this->option('table');
        if(is_array($table)){
            $table = $table[0];
        } elseif(is_null($table) || !$table){
            $table = '';
        }

        $stub = str_replace('name_table', strtolower($table), $stub);
        $data = DB::select("SELECT c.name, c.is_identity, ty.name as type FROM sys.columns c JOIN sys.types ty ON ty.user_type_id = c.user_type_id WHERE [object_id] = OBJECT_ID('" . $table . "');");

        if (sizeof($data) <= 0) die("\e[1;37;41mTable not found!\e[0m\n");

        $fields = null;
        $types = "/**\n";
        $types .= " * " . $this->getNamespace($name) . "\n";
        $types .= " *\n";

        foreach ($data as $key => $obj) {
            if ($key == 0) {
                $stub = str_replace('cod_primary', trim(strtolower($obj->name)), $stub);
                if ($obj->is_identity == 0) $stub = str_replace('{{incrementing}}', "\n    public    \$incrementing = false;", $stub);
            }
            if ($obj->type != "int") {
                $stub = str_replace('{{keyType}}', "\n    protected \$keyType      = \"string\";", $stub);
            } else {
                $stub = str_replace('{{keyType}}', '', $stub);
                $stub = str_replace('{{incrementing}}', '', $stub);
            }
            $fields .= "\n        '" . trim(strtolower($obj->name)) . "',";
            $types .= " * @property {$this->typeSqlToPhp(trim($obj->type))} \$" . trim(strtolower($obj->name)) . ";\n";
        }
        $types .= "**/\n";

        $stub = str_replace('field', $fields, $stub);
        $stub = str_replace('{{types}}', $types, $stub);

        return $this;
    }

    /**
     * Filtro type dados
     *
     * @param string $type
     * @return string
     */
    public function typeSqlToPhp($type)
    {
        switch ($type) {
            case in_array($type, ['char', 'varchar', 'datetime', 'date', 'nvarchar']):
                $type = "string";
                break;
            case in_array($type, ['smallint', 'bit', 'tinyint']):
                $type = "int";
                break;
            case in_array($type, ['double', 'decimal']):
                $type = "float";
                break;
            default:
                break;
        }

        return $type;
    }

    /**
     * Obtpem o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/Stubs/generate-model.stub';
    }
    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Models';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model.'],
            ['--queue', InputArgument::REQUIRED, 'The name of the table.'],
        ];
    }
}
