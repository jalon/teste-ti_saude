## Teste TI-SAUDE

Proposta:
Desenvolver a partir de modelo ER uma API utilizando a linguagem PHP
com o framework Laravel implementado JWT como método de autenticação.

# Pre-requisitos
``` PHP 8 or above ```<br>
``` MySql ```

# Instalação
* Database <br>
    ``` Diretorio database: Abrir modelo MySql Workbench (db.mwb) ou importar o SQL (schema.sql) ``` <br>
    ``` Alterar DB_USERNAME e DB_PASSWORD no .env ```
    

* Projeto <br>
    ``` composer i ```

# Start
``` php artisan serve ```

# Test
``` php artisan test ```

# Collection Postman
[Link para o arquivo .json](storage/tiSaude.postman_collection.json)
